package com.example.chapter10.util;

import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.util.Log;

//import com.example.chapter10.BuildConfig;

public class LogUtil {
    public static boolean isDebug = false; // 标志位需要在启动App时同步

    public static void v(String tag, String msg) {
        if (isDebug) {
            Log.v(tag, msg); // 打印冗余日志
        }
    }

    public static void d(String tag, String msg) {
        if (isDebug) {
            Log.d(tag, msg); // 打印调试日志
        }
    }

    public static void i(String tag, String msg) {
        if (isDebug) {
            Log.i(tag, msg); // 打印一般日志
        }
    }

    public static void w(String tag, String msg) {
        if (isDebug) {
            Log.w(tag, msg); // 打印警告日志
        }
    }

    public static void e(String tag, String msg) {
        if (isDebug) {
            Log.e(tag, msg); // 打印错误日志
        }
    }
}
