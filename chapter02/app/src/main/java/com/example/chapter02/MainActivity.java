package com.example.chapter02;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    protected void onResume(){
        super.onResume();
        geNextPage(); //跳转到下个页面
    }

    //跳转到下个页面
    private void geNextPage(){
        TextView tv_hello = findViewById(R.id.tv_hello);
        tv_hello.setText("3秒后进入下个界面");
        //延时3秒（3000毫秒）后启动任务mGoNext
        new Handler().postDelayed(mGoNext, 3000);
    }

    private Runnable mGoNext = new Runnable() {
        @Override
        public void run() {
            //活动页面跳转，从MainActivity跳转到Main2Activity
            startActivity(new Intent(MainActivity.this, MainActivity2.class));
        }
    };
}