package com.example.chapter06;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.example.chapter06.util.ToastUtil;
import com.example.chapter06.util.ViewUtil;

import java.util.Random;

public class LoginShareActivity extends AppCompatActivity {
    private RadioGroup rg_login; // 声明一个单选组对象
    private RadioButton rb_password; // 声明一个单选按钮对象
    private RadioButton rb_verifycode; // 声明一个单选按钮对象
    private EditText et_phone; // 声明一个编辑框对象
    private TextView tv_password; // 声明一个文本视图对象
    private EditText et_password; // 声明一个编辑框对象
    private Button btn_forget; // 声明一个按钮控件对象
    private CheckBox ck_remember; // 声明一个复选框对象
    private int mRequestCode = 0; // 跳转页面时的请求码
    private boolean isRemember = false; // 是否记住密码
    private String mPassword = "111111"; // 默认密码
    private String mVerifyCode; // 验证码
    private SharedPreferences mShared; // 声明一个共享参数对象

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_share);
        rg_login = findViewById(R.id.rg_login);
        rb_password = findViewById(R.id.rb_password);
        rb_verifycode = findViewById(R.id.rb_verifycode);
        et_phone = findViewById(R.id.et_phone);
        tv_password = findViewById(R.id.tv_password);
        et_password = findViewById(R.id.et_password);
        btn_forget = findViewById(R.id.btn_forget);
        ck_remember = findViewById(R.id.ck_remember);
        // 给rg_login设置单选监听器
        rg_login.setOnCheckedChangeListener(new RadioListener());
        // 给ck_remember设置勾选监听器
        ck_remember.setOnCheckedChangeListener((buttonView, isChecked)->{isRemember = isChecked;});
        // 给et_phone添加文本监听器
        et_phone.addTextChangedListener(new HideTextWatcher(et_phone, 11));
        // 给et_password添加文本监听器
        et_password.addTextChangedListener(new HideTextWatcher(et_password, 6));
        btn_forget.setOnClickListener(new MyOnClickListener());
        findViewById(R.id.btn_login).setOnClickListener(new MyOnClickListener());
        // 从share_login.xml获取共享对象参数
        mShared = getSharedPreferences("share_login", MODE_PRIVATE);
        // 获取共享参数保存的手机号码
        String phone = mShared.getString("phone", "");
        // 获取共享参数保存的密码
        String password = mShared.getString("password", "");
        et_phone.setText(phone); // 往手机号码编辑框填写上次保存的手机号
        et_password.setText(password); // 往密码编辑框填写上次保存的密码
    }

    // 定义登录方式的单选监听器
    private class RadioListener implements RadioGroup.OnCheckedChangeListener {
        @Override
        public void onCheckedChanged(RadioGroup radioGroup, int checkedId) {
            if (R.id.rb_password == checkedId) { // 选择密码登录
                tv_password.setText("登录密码：");
                et_password.setHint("请输入密码");
                btn_forget.setText("忘记密码");
                ck_remember.setVisibility(View.VISIBLE);
            } else if (R.id.rb_verifycode == checkedId) { // 选择验证码登录
                tv_password.setText("　验证码：");
                et_password.setHint("请输入验证码");
                btn_forget.setText("获取验证码");
                ck_remember.setVisibility(View.GONE);
            }
        }
    }
    // 定义一个编辑框监听器，在输入文本达到指定长度时自动隐藏输入法
    private class HideTextWatcher implements TextWatcher {
        private EditText mView; // 声明一个编辑框对象
        private int mMaxLength; // 声明一个最大长度变量
        public HideTextWatcher(EditText v, int maxLenght) {
            super();
            mView = v;
            mMaxLength = maxLenght;
        }
        // 在编辑框中输入文本变化前触发
        @Override
        public void beforeTextChanged(CharSequence charSequence, int start, int count, int after) { }
        // 在编辑框文本输入时触发
        @Override
        public void onTextChanged(CharSequence charSequence, int start, int before, int count) { }
        // 在编辑框输入文本变化后触发
        @Override
        public void afterTextChanged(Editable editable) {
            String str = editable.toString(); // 获取已输入文本字符串
            if ((11 == str.length() && 11 == mMaxLength) || (6 == str.length() && 6 == mMaxLength)) {
                ViewUtil.hideOneInputMethod(LoginShareActivity.this, mView);
            }
        }
    }
    // 定义一个按钮监听器
    private class MyOnClickListener implements View.OnClickListener {
        @Override
        public void onClick(View view) {
            String phone = et_phone.getText().toString();
            if (R.id.btn_forget == view.getId()) { // 点击”忘记密码“按钮
                if (11 > phone.length()) { // 手机号不是11位
                    ToastUtil.show(LoginShareActivity.this, "请输入正确的手机号");
                    return;
                }
                if (rb_password.isChecked()) { // 选择了密码方式校验，此时跳到找回密码页面
                    // 携带手机号码跳转到找回密码页面
                    Intent intent = new Intent(LoginShareActivity.this, LoginForgetActivity.class);
                    intent.putExtra("phone", phone);
                    startActivityForResult(intent, mRequestCode); // 携带意图返回上一级界面
                } else if (rb_verifycode.isChecked()) { // 选择了验证码方式校验，此时要生成六位随机数字验证码
                    // 生成六位随机的验证码
                    mVerifyCode = String.format("%06d", new Random().nextInt(999999));
                    // 弹出提示对话框，提示用户记住六位密码验证数字
                    AlertDialog.Builder builder = new AlertDialog.Builder(LoginShareActivity.this);
                    builder.setTitle("请记住验证码");
                    builder.setMessage("手机号" + phone + "，本次验证码是" + mVerifyCode + "，请输入验证码");
                    builder.setPositiveButton("好的", null);
                    AlertDialog alert = builder.create();
                    alert.show();
                }
            } else if (R.id.btn_login == view.getId()) { // 点击了”登录按钮“
                if (11 > phone.length()) { // 手机号不足11位
                    ToastUtil.show(LoginShareActivity.this, "请输入正确的手机号");
                    return;
                }
                if (rb_password.isChecked()) { // 密码方式校验
                    if (!et_password.getText().toString().equals(mPassword)) {
                        ToastUtil.show(LoginShareActivity.this, "请输入正确的密码");
                    } else { // 密码校验通过
                        loginSuccess(); // 提示用户登录成功
                    }
                } else if (rb_verifycode.isChecked()) { // 验证码方式校验
                    if (!et_password.getText().toString().equals(mVerifyCode)) {
                        ToastUtil.show(LoginShareActivity.this, "请输入正确的验证码");
                    } else { // 验证码校验通过
                        loginSuccess(); // 提示用户登录成功
                    }
                }
            }
        }
    }
    // 从下个页面携带参数返回当前页面时触发
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // 用户密码已改为新密码，故更新密码变量
        mPassword = data.getStringExtra("new_password");
    }
    // 从修改密码页面返回登录页面，要清空密码输入框
    @Override
    protected void onRestart() {
        super.onRestart();
        et_password.setText("");
    }
    // 校验通过，登录成功
    private void loginSuccess() {
        String desc = String.format("您的手机号码是%s，恭喜你通过登录验证，点击“确定”按钮返回上个页面",
                et_phone.getText().toString());
        // 弹出提示框，提示用户登陆成功
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("登陆成功");
        builder.setMessage(desc);
        builder.setPositiveButton("确定返回", (dialog, which) -> finish());
        builder.setNegativeButton("我再看看", null);
        AlertDialog alert = builder.create();
        alert.show();
        // 如果勾选了”记住密码“，就把手机号码和密码都保存到共享参数中
        if (isRemember) {
            SharedPreferences.Editor editor = mShared.edit(); // 获取编辑器对象
            editor.putString("phone", et_phone.getText().toString()); // 添加名叫phone的手机号码
            editor.putString("password", et_password.getText().toString()); // 添加名叫password的密码
            editor.commit(); // 提交编辑器中的修改
        }
    }
}