package com.example.chapter05;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class AlertDialogActivity extends AppCompatActivity implements View.OnClickListener{
    private TextView tv_alert; // 声明一个视图对象
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alert_dialog);
        // 从布局中获取文本视图对象tv_alert
        tv_alert = findViewById(R.id.tv_alert);
        // 注册按钮监听器
        findViewById(R.id.btn_alert).setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if(R.id.btn_alert == view.getId()) {
            // 创建提示对话框的建造器
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("尊敬的用户"); // 设对话框标题
            builder.setMessage("你真的要卸载我吗？"); // 设置对话框的提示内容
            // 设置对话框的肯定按钮文本及其监听器
            builder.setPositiveButton("残忍卸载", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int which) {
                    tv_alert.setText("虽然依依不舍，但是只能离开了");
                }
            });
            // 设置对话框的否定按钮文本及其监听器
            builder.setNegativeButton("我再想想", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int which) {
                    tv_alert.setText("让我再陪你三百六十五个日夜");
                }
            });
            AlertDialog alert = builder.create(); // 根据构造器构建提醒对话框对象
            alert.show();
        }
    }
}