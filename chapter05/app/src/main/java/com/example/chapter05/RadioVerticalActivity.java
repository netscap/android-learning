package com.example.chapter05;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.RadioGroup;
import android.widget.TextView;

public class RadioVerticalActivity extends AppCompatActivity implements RadioGroup.OnCheckedChangeListener {
    private TextView tv_marry; // 声明一个文本视图对象
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_radio_vertical);
        // 从布局文件中获取控件tv_marry
        tv_marry = findViewById(R.id.tv_marry);
        // 注册监听器，用户点击单选按钮，就触发onCheckChanged方法
        ((RadioGroup)findViewById(R.id.rg_marry)).setOnCheckedChangeListener(this);
    }

    @Override
    public void onCheckedChanged(RadioGroup radioGroup, int checkedId) {
        if (R.id.rb_married == checkedId) {
            tv_marry.setText("当前已婚");
        } else if (R.id.rb_unmarried == checkedId) {
            tv_marry.setText("当前未婚");
        }
    }
}