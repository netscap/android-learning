package com.example.chapter05;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

public class RadioHorizontalActivity extends AppCompatActivity implements RadioGroup.OnCheckedChangeListener {
    private TextView tv_sex; // 声明一个文本视图对象
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_radio_horizontal);
        // 从布局文件中获取视图控件tv_sex
        tv_sex = findViewById(R.id.tv_sex);
        // 注册监听器，一旦点击组内单选按钮，就触发监听器的onCheckChanged方法
        ((RadioGroup)findViewById(R.id.rg_sex)).setOnCheckedChangeListener(this);
    }

    @Override
    public void onCheckedChanged(RadioGroup radioGroup, int checkedId) {
        if (R.id.rb_male == checkedId) {
            tv_sex.setText("性别为男");
        } else if (R.id.rb_female == checkedId) {
            tv_sex.setText("性别为女");
        }
    }
}