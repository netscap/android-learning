package com.example.chapter05;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;

import com.example.chapter05.util.ViewUtil;

public class EditHideActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_hide);
        // 获取编辑框et_phone
        EditText et_phone = findViewById(R.id.et_phone);
        // 获取编辑框et_password
        EditText et_password = findViewById(R.id.et_password);
        // 给手机号编辑框添加文本变化监听器
        et_phone.addTextChangedListener(new HideTextWatcher(et_phone, 11));
        // 给密码编辑框添加文本变化监听器
        et_password.addTextChangedListener(new HideTextWatcher(et_password, 6));
    }

    // 定义一个编辑框监听器，在输入文本达到指定长度时自动隐藏输入法
    private class HideTextWatcher implements TextWatcher {
        private EditText mView; // 声明一个编辑框对象
        private int mMaxLength; // 声明一个最大长度变脸
        HideTextWatcher(EditText editText, int maxLength) {
            super();
            mView = editText;
            mMaxLength = maxLength;
        }

        // 在编辑框的输入文本变化前触发
        @Override
        public void beforeTextChanged(CharSequence charSequence, int start, int count, int after) {

        }

        // 在编辑框的输入文本变化时触发
        @Override
        public void onTextChanged(CharSequence charSequence, int start, int before, int count) {

        }

        // 在编辑框的输入文本变化后触发
        @Override
        public void afterTextChanged(Editable editable) {
            String str = editable.toString(); // 获取已输入的文本字符
            // 输入字符串长11位（手机号），或者达6位（密码）时关闭输入法
            if ((11 == str.length() && 11 == mMaxLength) || (6 == str.length() && 6 == mMaxLength)) {
                ViewUtil.hideOneInputMethod(EditHideActivity.this, mView);
            }
        }
    }
}