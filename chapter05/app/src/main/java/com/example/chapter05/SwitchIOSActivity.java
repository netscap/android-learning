package com.example.chapter05;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

public class SwitchIOSActivity extends AppCompatActivity implements CompoundButton.OnCheckedChangeListener {
    private CheckBox ck_status; // 声明一个复选对象
    private TextView tv_result; // 声明一个文本视图对象
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_switch_iosactivity);
        // 从布局文件中获取名叫sw_status的开关按钮
        ck_status = findViewById(R.id.ck_status);
        // 从布局文件中获取名叫tv_result的文本视图
        tv_result = findViewById(R.id.tv_result);
        // 给复选框注册监听器，一旦用户点击它，就触发监听器的onCheckedChanged方法
        ck_status.setOnCheckedChangeListener(this);
    }
    // 刷新仿iOS按钮的开关说明
    private void refreshResult() {
        String desc = String.format("仿iOS开关的状态是%s", ck_status.isChecked()? "开":"关");
        tv_result.setText(desc);
    }
    @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
        refreshResult();
    }
}