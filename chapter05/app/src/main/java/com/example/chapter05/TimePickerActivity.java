package com.example.chapter05;

import androidx.appcompat.app.AppCompatActivity;

import android.app.TimePickerDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.TimePicker;

import java.util.Calendar;

// 该页面类实现了接口OnTimeSetListener，意味着要重写时间监听器的onTimeSet方法
public class TimePickerActivity extends AppCompatActivity implements
        View.OnClickListener, TimePickerDialog.OnTimeSetListener {
    private TextView tv_time; // 声明一个文本对象
    private TimePicker tp_time; // 声明一个时间选择器对象
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_time_picker);
         // 从布局文件中获取文本对象tv_time
        tv_time = findViewById(R.id.tv_time);
        // 从布局文件中获取时间选择器对象tp_time
        tp_time = findViewById(R.id.tp_time);
        findViewById(R.id.btn_time).setOnClickListener(this);
        findViewById(R.id.btn_ok).setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (R.id.btn_time == view.getId()) {
            // 获取日历的一个实例，里面包含当前的分秒
            Calendar calendar = Calendar.getInstance();
            // 创建一个对话框，该对话框已经集成了时间选择器
            // TimePickerDialog的第二个参数指定了时间监听器
            TimePickerDialog dialog = new TimePickerDialog(this, this,
                    calendar.get(Calendar.HOUR_OF_DAY), // 小时
                    calendar.get(Calendar.HOUR_OF_DAY), // 分钟
                    true);  // 使用24小时制；false表示12小时制
            dialog.show(); // 显示时间对话框
        } else if (R.id.btn_ok == view.getId()) {
            // 获取定时器tp_time的时间
            String desc = String.format("您选择的时间是%d时%d分",
                    tp_time.getHour(), tp_time.getMinute());
            tv_time.setText(desc);
        }
    }

    // 一旦点击时间对话框上的确定按钮，就会触发监听器的onTimeSet方法
    @Override
    public void onTimeSet(TimePicker timePicker, int hourOfDay, int minute) {
        // 获取时间对话框设定的小时和分钟
        String desc = String.format("您选择的时间是%d时%d分", hourOfDay, minute);
        tv_time.setText(desc);
    }
}