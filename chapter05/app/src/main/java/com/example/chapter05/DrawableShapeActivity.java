package com.example.chapter05;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;

public class DrawableShapeActivity extends AppCompatActivity implements View.OnClickListener{
    private View v_content; // 声明一个视图对象
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_drawable_shape);
        v_content = findViewById(R.id.v_content);
        // v_content背景设置为圆角矩形
        v_content.setBackgroundResource(R.drawable.shape_rect_gold);
        findViewById(R.id.btn_rect).setOnClickListener(this);
        findViewById(R.id.btn_oval).setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if(view.getId() == R.id.btn_rect) { // 点击了"圆角矩形背景按钮"
            // v_content的背景设置为圆角矩形
            v_content.setBackgroundResource(R.drawable.shape_rect_gold);
        } else if(view.getId() == R.id.btn_oval) { // 点击了"圆角矩形背景按钮"
            // v_content的背景设置为椭圆形状
            v_content.setBackgroundResource(R.drawable.shape_oval_rose);
        }
    }
}