package com.example.chapter03;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.chapter03.util.DateUtil;

public class ButtonClickActivity extends AppCompatActivity implements View.OnClickListener{
    //声明tv_result视图实例
    private TextView tv_result;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_button_click);
        //获取名为tv_result的文本视图
        tv_result = findViewById(R.id.tv_result);
        //获取名为btn_click_single的按键控件并注册监听器
        findViewById(R.id.btn_click_single).setOnClickListener(new MyOnClickListener());
        //获取名为btn_click_public的按键控件并注册监听器
        findViewById(R.id.btn_click_public).setOnClickListener(this);
    }

    //定义一个实现View.OnClickListener接口的监听器
    class MyOnClickListener implements View.OnClickListener{
        @Override
        public void onClick(View view) { //点击事件的处理方法
            String desc = String.format("%s 您点击了按钮：%s",
                    DateUtil.getNowTime(), ((Button) view).getText());
            tv_result.setText(desc); // 设置文本视图的文本内容
        }
    }

    @Override
    public void onClick(View view) { //点击事件处理方法
        //判断是否来自btn_click_public按键
        if(view.getId() == R.id.btn_click_public){
            String desc = String.format("%s 您点击了按钮：%s",
                    DateUtil.getNowTime(), ((Button) view).getText());
            tv_result.setText(desc); // 设置文本视图的文本内容
        }
    }
}