package com.example.chapter03;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.chapter03.util.DateUtil;

public class ButtonEnableActivity extends AppCompatActivity implements View.OnClickListener{
    //声明一个名为tv_result的文本实例
    private TextView tv_result;
    //声明一个名为btn_test的按钮实例
    private Button btn_test;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_button_enable);
        //获取名为tv_result的文本视图
        tv_result = findViewById(R.id.tv_result);
        //对按钮注册监听器
        findViewById(R.id.btn_enable).setOnClickListener(this);
        findViewById(R.id.btn_disable).setOnClickListener(this);
        //获取名为btn_test的按钮控件
        btn_test = findViewById(R.id.btn_test);
        btn_test.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        // 点击了按钮“启用测试按钮”
        if (view.getId() == R.id.btn_enable) { //点击了按钮“启用测试按钮”
            btn_test.setTextColor(Color.BLACK); //设置按钮的文字颜色
            btn_test.setEnabled(true); // 启用当前控件
        } else if(view.getId() == R.id.btn_disable) { //点击了按钮“禁用测试按钮”
            btn_test.setTextColor(Color.GRAY); // 设置按钮的文字颜色
            btn_test.setEnabled(false);//禁用当前控件
        } else if (view.getId() == R.id.btn_test) { // 点击了按钮“测试按钮”
            String desc = String.format("%s 您点击了按钮：%s",
                    DateUtil.getNowTime(), ((Button) view).getText());
            tv_result.setText(desc); // 设置文本视图的文本内容
        }
    }
}