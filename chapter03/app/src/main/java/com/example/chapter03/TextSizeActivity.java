package com.example.chapter03;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.translation.ViewTranslationCallback;
import android.widget.TextView;

public class TextSizeActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_text_size);
        //从布局中获取名为tv_sp的文本视图
        TextView tv_sp = findViewById(R.id.tv_sp);
        tv_sp.setTextSize(30); //设置tv_sp的文本大小
    }
}