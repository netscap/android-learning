package com.example.chapter03.util;

import android.content.Context;
import androidx.annotation.NonNull;

public class Utils {
    //根据手机的分辨率从dp的单位转成px（像素）
    public static int dip2px(@NonNull Context context, float dpValue){
        //获取当前手机的像素密度（1个dp对应几个px）
        float scale = context.getResources().getDisplayMetrics().density;
        return (int) (dpValue*scale + 0.5f);
    }

    //根据手机分辨率从px（像素）的单位转成dp
    public static int px2dip(@NonNull Context context, float pxValue){
        //获取当前手机的像素密度（1个dp对应几个px）
        float scale = context.getResources().getDisplayMetrics().density;
        return (int) (pxValue/scale + 0.5f);
    }
}
