package com.example.chapter03;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

public class ImageScaleActivity extends AppCompatActivity implements View.OnClickListener{
    // 声明图像视图对象
    private ImageView iv_scale;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_scale);
        // 获取名为iv_scale的图像视图对象
        iv_scale = findViewById(R.id.iv_scale);
        // 注册按键监听器演示不同缩放类型效果
        findViewById(R.id.btn_center).setOnClickListener(this);
        findViewById(R.id.btn_fitCenter).setOnClickListener(this);
        findViewById(R.id.btn_centerCrop).setOnClickListener(this);
        findViewById(R.id.btn_centerInside).setOnClickListener(this);
        findViewById(R.id.btn_fitXY).setOnClickListener(this);
        findViewById(R.id.btn_fitStart).setOnClickListener(this);
        findViewById(R.id.btn_fitEnd).setOnClickListener(this);
    }

    @Override
    public void onClick(View view) { // 点击事件处理方法
        // 设置图像视图的图片资源
        iv_scale.setImageResource(R.drawable.apple);
        if(view.getId() == R.id.btn_center) {
            // 将缩放类型设置为“按照原尺寸居中显示”
            iv_scale.setScaleType(ImageView.ScaleType.CENTER);
        } else if (view.getId() == R.id.btn_fitCenter) {
            //将缩放类型设置为“保持宽高比例，缩放图片使其位于视图中间”
            iv_scale.setScaleType(ImageView.ScaleType.FIT_CENTER);
        } else if (view.getId() == R.id.btn_centerCrop) {
            // 将缩放类型设置为“缩放图片使其充满视图，并位于视图中间”
            iv_scale.setScaleType(ImageView.ScaleType.CENTER_CROP);
        } else if (view.getId() == R.id.btn_centerInside) {
            // 将缩放类型设置为“保持宽高比例，缩小图片使之位于视图中间（只缩小不放大）”
            iv_scale.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
        } else if (view.getId() == R.id.btn_fitXY) {
            // 将缩放类型设置为“缩放图片使其正好填满视图（图片可能被缩放变形）”
            iv_scale.setScaleType(ImageView.ScaleType.FIT_XY);
        } else if (view.getId() == R.id.btn_fitStart) {
            // 将缩放类型设置为“保持宽高比例，缩放图片使其位于视图上方或左侧”
            iv_scale.setScaleType(ImageView.ScaleType.FIT_START);
        } else if (view.getId() == R.id.btn_fitEnd) {
            // 将缩放类型设置为“保持宽高比例，缩放图片使其位于视图下方或右侧”
            iv_scale.setScaleType(ImageView.ScaleType.FIT_END);
        }
    }
}