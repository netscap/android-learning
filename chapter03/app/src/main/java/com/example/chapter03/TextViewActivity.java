package com.example.chapter03;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

public class TextViewActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_text_view);
        //获取名为tv_hello的文本视图
        TextView tv_hello = findViewById(R.id.tv_hello);
        //设置tv_text内容
        tv_hello.setText(R.string.hello);

//        tv_hello.setText("你好，世界");
    }
}