package com.example.chapter03;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.chapter03.util.DateUtil;

public class ButtonLongclickActivity extends AppCompatActivity implements View.OnLongClickListener{
    //定义一个tv_result文本视图
    private TextView tv_result;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_button_longclick);
        //获取名为tv_result的文本控件
        tv_result = findViewById(R.id.tv_result);
        //获取名为btn_longclick_single的按键控件并注册长按监听器
        findViewById(R.id.btn_longclick_single).setOnLongClickListener(new MyOnLongClickListener());
        //获取名为btn_longclick_public的按键并注册长按监听器
        findViewById(R.id.btn_longclick_public).setOnLongClickListener(this);
    }

    //定义一个实现View.OnLongClickListener接口的长按监听器
    class MyOnLongClickListener implements View.OnLongClickListener{
        @Override
        public boolean onLongClick(View view) { //长按事件的处理方法
            String desc = String.format("%s 您长按了按钮：%s",
                    DateUtil.getNowTime(), ((Button) view).getText());
            tv_result.setText(desc); // 设置文本视图的文本内容
            return true;
        }
    }

    //实现长按监听方法
    @Override
    public boolean onLongClick(View view) {
        if (view.getId() == R.id.btn_longclick_public) { // 来自按钮btn_longclick_public
            String desc = String.format("%s 您长按了按钮：%s",
                    DateUtil.getNowTime(), ((Button) view).getText());
            tv_result.setText(desc); // 设置文本视图的文本内容
        }
        return true;
    }
}