package com.example.chapter11;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Looper;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.widget.TextView;

import com.example.chapter11.util.DateUtil;

@SuppressLint("DefaultLocale")
public class KeyHardActivity extends AppCompatActivity {
    private static final String TAG = "KeyHardActivity";
    private TextView tv_result; // 声明一个文本视图对象
    private String desc = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_key_hard);
        tv_result = findViewById(R.id.tv_result);
    }

    // 在发生物理按键动作时触发
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        desc = String.format("%s物理按键的编码是%d", desc, keyCode);
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            desc = String.format("%s，按键为返回键", desc);
            // 延迟3秒后启动页面关闭任务
            new Handler(Looper.myLooper()).postDelayed(() -> finish(), 3000);
        } else if (keyCode == KeyEvent.KEYCODE_VOLUME_UP) {
            desc = String.format("%s，按键为加大音量键", desc);
        } else if (keyCode == KeyEvent.KEYCODE_VOLUME_DOWN) {
            desc = String.format("%s，按键为减小音量键", desc);
        }
        desc = desc + "\n";
        tv_result.setText(desc);
        // 返回true表示不再响应系统动作，返回false表示继续响应系统动作
        return true;
    }
}
