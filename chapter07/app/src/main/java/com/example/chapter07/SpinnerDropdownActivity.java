package com.example.chapter07;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

public class SpinnerDropdownActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_spinner_dropdown);
        initSpinnerForDropdown(); // 初始化下拉模式的列表框
    }

    // 初始化下拉模式的列表框
    private void initSpinnerForDropdown() {
        // 声明一个下拉表的数组适配器
        ArrayAdapter<String> starAdapter = new ArrayAdapter<>(this, R.layout.item_select, starArray);
        // 从布局文件中获取名为sp_dropdown的下拉框
        Spinner sp_dropdown = findViewById(R.id.sp_dropdown);
        // 设置下拉框的标题。对话框模式才显示标题，下拉模式不显示标题
        sp_dropdown.setPrompt("请选择行星");
        // 设置下拉框数组适配器
        sp_dropdown.setAdapter(starAdapter);
        // 设置下拉框默认选择第一个
        sp_dropdown.setSelection(0);
        // 给下拉框注册监听器，一旦用户选择某一项，就出发监听器的onItemSelected方法
        sp_dropdown.setOnItemSelectedListener(new MySelectedListener());
    }

    // 定义下拉表需要显示的文本数组
    private String[] starArray = {"水星", "金星", "地球", "火星", "木星", "土星"};

    // 定义一个选择监听器，它实现接口OnItemSelectedListener
    class MySelectedListener implements AdapterView.OnItemSelectedListener {

        @Override
        public void onItemSelected(AdapterView<?> adapterView, View view, int index, long l) {
            // 选择事件的处理方法，其中index代表选择项的序号
            Toast.makeText(SpinnerDropdownActivity.this, "你选择的是" + starArray[index],
                    Toast.LENGTH_LONG).show();
        }

        @Override
        public void onNothingSelected(AdapterView<?> adapterView) { }
    }
}