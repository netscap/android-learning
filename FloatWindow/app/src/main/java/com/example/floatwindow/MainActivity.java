package com.example.floatwindow;

import android.os.Bundle;
import android.view.Gravity;
import android.widget.Toast;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;

import com.example.floatwindow.utils.AuthorityUtil;
import com.example.floatwindow.widget.FloatWindow;

public class MainActivity extends AppCompatActivity {
    private static FloatWindow mFloatWindow; // 声明一个悬浮窗对象

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        setContentView(R.layout.activity_main);
        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main), (v, insets) -> {
            Insets systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars());
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom);
            return insets;
        });

        // AndroidManifest.xml要先声明悬浮窗权限SYSTEM_ALERT_WINDOW
        // AppOpsManager.OP_SYSTEM_ALERT_WINDOW是隐藏变量（值为24），不能直接引用
        if (!AuthorityUtil.checkOp(this, 24)) { // 未开启悬浮窗权限
            Toast.makeText(this, "请先给该应用开启悬浮窗权限", Toast.LENGTH_SHORT).show();
            // 跳到悬浮窗权限的设置页面
            AuthorityUtil.requestAlertWindowPermission(this);
        }

        // 显示悬浮窗
        openFloatWindow();
    }

    /*
    * @brife 打开悬浮窗
    * */
    private void openFloatWindow() {
        if (mFloatWindow == null) {
            // 创建一个新的悬浮窗
            mFloatWindow = new FloatWindow(MainApplication.getInstance());
            // 设置悬浮窗的布局内容
            mFloatWindow.setLayout(R.layout.float_notice);
            // 设置悬浮窗的点击监听器
            mFloatWindow.setOnFloatListener(v -> mFloatWindow.close());
        }
        if (mFloatWindow != null && !mFloatWindow.isShow()) {
            mFloatWindow.show(Gravity.LEFT | Gravity.TOP); // 显示悬浮窗
        }
    }

    /*
    * @brife 关闭悬浮窗
    * */
    private void closeFloatWindow() {
        if (mFloatWindow != null && mFloatWindow.isShow()) {
            mFloatWindow.close(); // 关闭悬浮窗
        }
    }
}