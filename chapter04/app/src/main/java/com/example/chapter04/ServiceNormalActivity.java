package com.example.chapter04;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.example.chapter04.service.NormalService;
import com.example.chapter04.util.DateUtil;

import org.jetbrains.annotations.NotNull;

public class ServiceNormalActivity extends AppCompatActivity implements View.OnClickListener{
    private static TextView tv_normal;
    private Intent mIntent; // 声明一个意图对象
    private static String mDesc = "";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_service_normal);
        tv_normal = findViewById(R.id.tv_normal);
        findViewById(R.id.btn_start).setOnClickListener(this);
        findViewById(R.id.btn_stop).setOnClickListener(this);
        // 创建一个通往普通服务的意图
        mIntent = new Intent(this, NormalService.class);
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.btn_start) { // 点击启动服务按钮
            startService(mIntent); // 启动指定意图的服务
        } else if (view.getId() == R.id.btn_stop) { // 点击停止服务按钮
            stopService(mIntent); // 停止指定意图服务
        }
    }

    public static void showText(@NotNull String desc) {
        if (null != desc) {
            mDesc = String.format("%s%s %s\n", mDesc, DateUtil.getNowDateTime("HH:mm:ss"), desc);
            tv_normal.setText(mDesc);
        }
    }
}