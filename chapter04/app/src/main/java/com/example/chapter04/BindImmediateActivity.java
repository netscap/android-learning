package com.example.chapter04;

import androidx.appcompat.app.AppCompatActivity;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.example.chapter04.service.BindImmediateService;
import com.example.chapter04.util.DateUtil;

public class BindImmediateActivity extends AppCompatActivity implements View.OnClickListener{
    private final static String TAG = "BindImmediateActivity";
    private static TextView tv_immediate; // 声明一个文本视图
    private Intent mIntent; // 声明一个意图对象
    private static String mDesc = ""; // 日志描述
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bind_immediate);
        tv_immediate = findViewById(R.id.tv_immediate);
        findViewById(R.id.btn_start_bind).setOnClickListener(this);
        findViewById(R.id.btn_unbind).setOnClickListener(this);
        // 创建一个通往立即绑定服务的意图
        mIntent = new Intent(this, BindImmediateService.class);
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.btn_start_bind) { // 点击绑定服务按钮
            // 绑定服务。如果服务未启动，则系统先启动服务在进行绑定
            boolean bindFlag = bindService(mIntent, mServiceConn, Context.BIND_AUTO_CREATE);
            Log.d(TAG, "bindFlag=" + bindFlag);
        } else if (view.getId() == R.id.btn_unbind && null != mBindService) { // 点击解绑服务按钮
            // 解绑服务。如果先前服务立即绑定，则此时解绑之后自动停止服务
            unbindService(mServiceConn);
        }
    }

    public static void showText(String desc) {
        if (null != tv_immediate) {
            mDesc = String.format("%s%s %s\n", mDesc, DateUtil.getNowDateTime("HH:mm:ss"), desc);
            tv_immediate.setText(mDesc);
        }
    }

    // 声明一个服务对象
    private BindImmediateService mBindService;
    private ServiceConnection mServiceConn = new ServiceConnection() {
        // 获取服务对象时的操作
        @Override
        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            // 如果服务运行于另一个进程，则不能直接强制转换类型，否则会报错
            mBindService = ((BindImmediateService.LocalBinder) iBinder).getService();
            Log.d(TAG, "onServiceConnected");
        }
        // 无法获取对象时的操作
        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            mBindService = null;
            Log.d(TAG, "onServiceDisconnected");
        }
    };
}