package com.example.chapter04;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.example.chapter04.util.DateUtil;

public class ActRequestActivity extends AppCompatActivity implements View.OnClickListener{
    private String mRrequest = "你吃饭了吗？来我家吃吧";

    private TextView tv_response; // 声明一个文本视图对象
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_act_request);
        // 从布局文件中获取mRrequest文本视图
        tv_response = findViewById(R.id.tv_response);
        ((TextView)findViewById(R.id.tv_request)).setText("待发送的消息为："+mRrequest);
        findViewById(R.id.btn_request).setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if(view.getId() == R.id.btn_request) {
            // 创建一个意向，准备跳转指定活动页面
            Intent intent = new Intent(this, ActResponseActivity.class);
            Bundle bundle = new Bundle(); // 创建一个新包裹
            // 往包裹存入一个名为request_time的字符串
            bundle.putString("request_time", DateUtil.getNowTime());
            // 往包裹存入一个名为request_content的字符串
            bundle.putString("request_content", mRrequest);
            intent.putExtras(bundle); // 把包裹塞入意图
            // 期望接收下个页面返回数据，第二个参数为本次请求代码
            startActivityForResult(intent, 0);
        }
    }

    // 从下一个页面携带参数返回时触发。其中requestCode为请求代码
    // resultCode为结果代码，intent为以下各页面返回的意图对象
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) { // 接收返回数据
        super.onActivityResult(requestCode, resultCode, intent);
        // 意图非空，且请求代码为之前传的0，结果代码也为成功
        if (null != intent && 0 == requestCode && Activity.RESULT_OK == resultCode) {
            Bundle bundle = intent.getExtras(); // 从返回的意图中获取包裹
            // 从包裹中获取名为response_time的字符串
            String response_time = bundle.getString("response_time");
            // 从包裹中获取名为response_content的字符串
            String response_content = bundle.getString("response_content");
            String desc = String.format("收到返回消息：\n应答时间为：%s\n应答内容为：%s", response_time, response_content);
            tv_response.setText(desc);
        }
    }
}