package com.example.chapter04;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.example.chapter04.util.DateUtil;

public class ActResponseActivity extends AppCompatActivity implements View.OnClickListener{
    private String mResponse = "我吃过了，还是你来我家吃";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_act_response);
        // 显示待返回信息
        ((TextView)findViewById(R.id.tv_response)).setText("待返回的信息为：" + mResponse);
        // 获取从上一个页面传递的包裹
        Bundle bundle = getIntent().getExtras();
        // 从包裹中获取名为request_time的字符串
        String request_time = bundle.getString("request_time");
        // 从包裹中获取名为request_content的字符串
        String request_content = bundle.getString("request_content");
        String desc = String.format("收到请求消息：\n请求时间为：%s\n请求内容为：%s", request_time, request_content);
        ((TextView)findViewById(R.id.tv_request)).setText(desc);
        // 注册监听器
        findViewById(R.id.btn_response).setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.btn_response) {
            Intent intent = new Intent(); // 创建一个意图
            Bundle bundle = new Bundle(); // 创建一个包裹
            // 往包裹中存入名为response_time的字符串
            bundle.putString("response_time", DateUtil.getNowTime());
            // 往包裹中存入名为response_content的字符串
            bundle.putString("response_content", mResponse);
            intent.putExtras(bundle); // 把包裹塞入intent
            //携带意图返回上一个页面，RESULT_OK表示处理成功
            setResult(Activity.RESULT_OK, intent);
            finish();
        }
    }
}